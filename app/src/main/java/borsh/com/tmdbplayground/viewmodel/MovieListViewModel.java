package borsh.com.tmdbplayground.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import borsh.com.tmdbplayground.model.network.models.MovieItem;
import borsh.com.tmdbplayground.model.network.models.MovieList;
import borsh.com.tmdbplayground.model.network.models.base.BaseResponse;
import borsh.com.tmdbplayground.utils.constants.MovieCategory;

public class MovieListViewModel extends BaseViewModel {

    private int currentPage = 1;
    private int totalPages = 1;

    private String searchCriteria;
    private MovieCategory category;
    private MovieCategory previousCategory;
    private List<MovieItem> items;

    private MutableLiveData<List<MovieItem>> movieListObservable;
    private MutableLiveData<MovieCategory> categoryObservable;

    public MovieListViewModel(@NonNull Application application) {
        super(application);
        movieListObservable = new MutableLiveData<>();
        categoryObservable = new MutableLiveData<>();
        items = new ArrayList<>();
    }

    public LiveData<List<MovieItem>> getMovieListObservable() {
        return movieListObservable;
    }

    public LiveData<MovieCategory> getCategoryObservable() {
        return categoryObservable;
    }

    //==============================================================================================


    public void searchMovies(String search) {
        isLoadingObservable.postValue(true);
        searchCriteria = search;

        items.clear();
        currentPage = 1;

        repo.getSearchMovie(searchCriteria, currentPage).observeForever(this::handleResponse);
    }

    public void selectCategory(MovieCategory category) {
        isLoadingObservable.postValue(true);

        searchCriteria = "";
        if (this.category != category) {
            currentPage = 1;
        }
        switch (category) {
            default:
            case POPULAR:
                repo.getMoviesPopular(currentPage).observeForever(this::handleResponse);
                break;
            case TOP_RATED:
                repo.getMoviesTopRated(currentPage).observeForever(this::handleResponse);
                break;
            case NOW_PLAYING:
                repo.getMoviesNowPlaying(currentPage).observeForever(this::handleResponse);
                break;
            case UPCOMING:
                repo.getMoviesUpcoming(currentPage).observeForever(this::handleResponse);
                break;
            case SEARCH:
                repo.getSearchMovie(searchCriteria, currentPage).observeForever(this::handleResponse);
                break;
        }
    }

    private void handleResponse(BaseResponse<MovieList> response) {
        isLoadingObservable.postValue(false);
        if (response.getError() != null) {
            errorObservable.postValue(response);
            return;
        }

        MovieList movieList = response.getResult();
        if (category != movieList.getCategory()) {
            category = movieList.getCategory();
            if (category != MovieCategory.SEARCH) {
                previousCategory = category.cloneCategory();
            }
            categoryObservable.postValue(category);

            items.clear();
            currentPage = 1;
        }

        totalPages = movieList.getTotalPages();
        appendItems(movieList.getResults());
        movieListObservable.postValue(items);
    }

    public void appendItems(List<MovieItem> itemsNew) {
        for (MovieItem newMovieItem : itemsNew) {
            if (!items.contains(newMovieItem)) {
                items.add(newMovieItem);
            }
        }
    }

    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void doPagination() {
        if (currentPage < totalPages) {
            currentPage++;
        }
        retry();
    }

    public void retry() {
        if (category == MovieCategory.SEARCH) {
            isLoadingObservable.postValue(true);
            repo.getSearchMovie(searchCriteria, currentPage).observeForever(this::handleResponse);
        } else {
            selectCategory(category);
        }
    }

    public void setPreviousCategory() {
        if (previousCategory == null) {
            previousCategory = MovieCategory.POPULAR;
        }

        selectCategory(previousCategory.cloneCategory());
    }
}
