package borsh.com.tmdbplayground.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import borsh.com.tmdbplayground.model.Repository;
import borsh.com.tmdbplayground.model.network.models.base.BaseResponse;

public abstract class BaseViewModel extends AndroidViewModel {

    protected Repository repo = Repository.getInstance();

    protected MutableLiveData<Boolean> isLoadingObservable;
    protected MutableLiveData<BaseResponse> errorObservable;

    public BaseViewModel(@NonNull Application application) {
        super(application);
        isLoadingObservable = new MutableLiveData<>();
        errorObservable = new MutableLiveData<>();
    }

    public LiveData<Boolean> getIsLoadingObservable() {
        return isLoadingObservable;
    }

    public MutableLiveData<BaseResponse> getErrorObservable() {
        return errorObservable;
    }

    public abstract void retry();
}
