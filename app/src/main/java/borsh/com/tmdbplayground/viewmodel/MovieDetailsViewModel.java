package borsh.com.tmdbplayground.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import borsh.com.tmdbplayground.model.network.models.MovieDetails;

public class MovieDetailsViewModel extends BaseViewModel {

    private MutableLiveData<MovieDetails> movieDetailsObservable;
    private int movieId;

    public MovieDetailsViewModel(@NonNull Application application) {
        super(application);
        this.movieDetailsObservable = new MutableLiveData<>();
    }

    public LiveData<MovieDetails> getMovieDetailsObservable() {
        return movieDetailsObservable;
    }

    public void fetchMovieDetails(int movieId) {
        this.movieId = movieId;
        isLoadingObservable.postValue(true);

        repo.getMovieDetails(movieId).observeForever(movieDetails -> {
            isLoadingObservable.postValue(false);
            movieDetailsObservable.postValue(movieDetails.getResult());
        });
    }

    @Override
    public void retry() {
        repo.getMovieDetails(movieId);
    }
}
