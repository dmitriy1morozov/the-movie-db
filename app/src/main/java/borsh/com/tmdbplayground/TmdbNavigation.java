package borsh.com.tmdbplayground;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavAction;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.FragmentNavigator;
import androidx.viewbinding.ViewBinding;

import borsh.com.tmdbplayground.utils.KeyboardUtils;
import timber.log.Timber;

public class TmdbNavigation {

    private ViewBinding layout;

    private AppCompatActivity activity;
    private int navHostId;

    public static TmdbNavigation initWihtFragment(ViewBinding layout) {
        return new TmdbNavigation(layout);
    }

    public static TmdbNavigation initWihtActivity(AppCompatActivity activity, int navHostId) {
        return new TmdbNavigation(activity, navHostId);
    }

    //==============================================================================================
    private TmdbNavigation() {
    }

    private TmdbNavigation(ViewBinding layout) {
        this.layout = layout;
    }

    private TmdbNavigation(AppCompatActivity activity, int navHostId) {
        this.activity = activity;
        this.navHostId = navHostId;
    }
    //==============================================================================================

    public void back() {
        KeyboardUtils.hideKeyboard(layout.getRoot());
        Navigation.findNavController(layout.getRoot()).popBackStack();
    }

    public void to(int actionId) {
        process(null, null, actionId, null, null);
    }

    public void to(int actionId, Bundle args) {
        process(null, null, actionId, args, null);
    }

    public void to(int actionId, Bundle args, FragmentNavigator.Extras extras) {
        process(null, null, actionId, args, extras);
    }

    public void to(int popTo, boolean inclusive, int actionId, Bundle args) {
        process(popTo, inclusive, actionId, args, null);
    }

    //==============================================================================================
    private void process(Integer popTo, Boolean inclusive, int actionId, Bundle args, FragmentNavigator.Extras extras) {
        if (!canNavigate(actionId)) {
            Timber.e("Navigation error: cannot navigate to the requested action");
            return;
        }
        hideKeyboard();

        NavOptions.Builder navOptionsBuilder = getNavOptionsBuilder();
        if (popTo != null && inclusive != null) {
            navOptionsBuilder.setPopUpTo(popTo, inclusive);
        }

        try {
            getNavController().navigate(actionId, args, navOptionsBuilder.build(), extras);
        } catch (IllegalStateException ise) {
            Timber.e("Navigation error: %s", ise.getMessage());
        }
    }


    private boolean canNavigate(int actionId) {
        if (layout == null) {
            return true;
        }

        NavDestination currentDestination = Navigation.findNavController(layout.getRoot()).getCurrentDestination();
        if (currentDestination == null) {
            return false;
        } else {
            NavAction initiatedAction = currentDestination.getAction(actionId);
            return initiatedAction != null;
        }
    }

    private void hideKeyboard() {
        if (layout != null) {
            KeyboardUtils.hideKeyboard(layout.getRoot());
        } else if (activity != null) {
            KeyboardUtils.hideKeyboard(activity);
        }
    }

    private NavController getNavController() throws IllegalStateException {
        if (layout != null) {
            return Navigation.findNavController(layout.getRoot());
        } else if (activity != null && navHostId != 0) {
            return Navigation.findNavController(activity, navHostId);
        } else {
            throw new IllegalStateException("Couldn't find navController. Either no View provided or activity not initialized correctly");
        }
    }

    private NavOptions.Builder getNavOptionsBuilder() {
        return new NavOptions.Builder()
                .setEnterAnim(R.anim.slide_in_right)
                .setExitAnim(R.anim.slide_out_left)
                .setPopEnterAnim(R.anim.slide_in_left)
                .setPopExitAnim(R.anim.slide_out_right);
    }
}
