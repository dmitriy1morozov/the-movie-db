package borsh.com.tmdbplayground.utils.constants;

public enum PosterSize {
    w92("w92"),
    w154("w154"),
    w185("w185"),
    w342("w342"),
    w500("w500"),
    w780("w780"),
    original("original");

    private String stringValue;

    private PosterSize(String toString) {
        stringValue = toString;
    }

    public static PosterSize newInstance(String stringValue) {
        switch (stringValue) {
            case "w92":
                return w92;
            case "w154":
                return w154;
            case "w185":
                return w185;
            case "w342":
                return w342;
            case "w500":
                return w500;
            case "w780":
                return w780;
            default:
            case "original":
                return original;
        }
    }

    @Override
    public String toString() {
        return stringValue;
    }
}
