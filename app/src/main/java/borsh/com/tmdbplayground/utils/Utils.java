package borsh.com.tmdbplayground.utils;

import android.graphics.Outline;
import android.view.View;
import android.view.ViewOutlineProvider;


public class Utils {
    public static void setRoundCorners(View view) {
        ViewOutlineProvider viewOutlineProvider = new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setRoundRect(0, 0, view.getWidth(), view.getHeight(), 40);
            }
        };
        view.setOutlineProvider(viewOutlineProvider);
        view.setClipToOutline(true);
    }
}
