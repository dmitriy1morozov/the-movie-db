package borsh.com.tmdbplayground.utils.constants;

public enum MovieCategory {

    POPULAR(1, "Popular"),
    TOP_RATED(2, "Top rated"),
    NOW_PLAYING(3, "Now Playing"),
    UPCOMING(4, "Upcoming"),
    SEARCH(5, "Search");

    private final String stringValue;
    private final int intValue;

    MovieCategory(int intValue, String stringValue) {
        this.stringValue = stringValue;
        this.intValue = intValue;
    }

    public static MovieCategory newInstance(int intValue) {
        switch (intValue) {
            default:
            case 1: return POPULAR;
            case 2: return TOP_RATED;
            case 3: return NOW_PLAYING;
            case 4: return UPCOMING;
            case 5: return SEARCH;
        }
    }

    public int getInt() {
        return intValue;
    }

    public String getString() {
        return stringValue;
    }

    public MovieCategory cloneCategory() {
        return newInstance(intValue);
    }
}
