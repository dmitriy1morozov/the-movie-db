package borsh.com.tmdbplayground.model.network;

import borsh.com.tmdbplayground.BuildConfig;
import borsh.com.tmdbplayground.model.network.models.MovieDetails;
import borsh.com.tmdbplayground.model.network.models.MovieList;
import borsh.com.tmdbplayground.model.network.models.base.BaseResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Rest {

    @GET("movie/popular?api_key=" + BuildConfig.REST_KEY)
    Observable<BaseResponse<MovieList>> getMoviesPopular(@Query("language") String language,
                                                         @Query("page") int page
    );

    @GET("movie/top_rated?api_key=" + BuildConfig.REST_KEY)
    Observable<BaseResponse<MovieList>> getMoviesTopRated(@Query("language") String language,
                                                          @Query("page") int page
    );

    @GET("movie/now_playing?api_key=" + BuildConfig.REST_KEY)
    Observable<BaseResponse<MovieList>> getMoviesNowPlaying(@Query("language") String language,
                                                            @Query("page") int page
    );

    @GET("movie/upcoming?api_key=" + BuildConfig.REST_KEY)
    Observable<BaseResponse<MovieList>> getMoviesUpcoming(@Query("language") String language,
                                                          @Query("page") int page
    );

    @GET("search/movie?api_key=" + BuildConfig.REST_KEY)
    Observable<BaseResponse<MovieList>> getMoviesSearch(@Query("query") String searchCriteria,
                                                        @Query("language") String language,
                                                        @Query("include_adult") boolean include_adult,
                                                        @Query("page") int page
    );

    @GET("movie/{movie_id}?api_key=" + BuildConfig.REST_KEY)
    Observable<BaseResponse<MovieDetails>> getMovieDetails(@Path("movie_id") int movieId);
}
