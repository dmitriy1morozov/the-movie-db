package borsh.com.tmdbplayground.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import borsh.com.tmdbplayground.BuildConfig;
import borsh.com.tmdbplayground.model.network.Rest;
import borsh.com.tmdbplayground.model.network.RestSubscriber;
import borsh.com.tmdbplayground.model.network.models.MovieDetails;
import borsh.com.tmdbplayground.model.network.models.MovieList;
import borsh.com.tmdbplayground.model.network.models.base.BaseResponse;
import borsh.com.tmdbplayground.utils.constants.MovieCategory;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Repository {

    private static Repository projectRepository;

    private static final int CONNECT_TIMEOUT = 60;
    private static final int WRITE_TIMEOUT = 60;
    private static final int READ_TIMEOUT = 60;

    private static final String REST_LANGUAGE = "en-US";

    private Rest rest;

    public synchronized static Repository getInstance() {
        if (projectRepository == null) {
            projectRepository = new Repository();
        }
        return projectRepository;
    }

    private Repository() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BuildConfig.REST_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getOkHttpClient())
                .build();
        rest = retrofit.create(Rest.class);
    }

    //============= REST interface =================================================================

    public MutableLiveData<BaseResponse<MovieDetails>> getMovieDetails(int movieId) {
        final MutableLiveData<BaseResponse<MovieDetails>> liveData = new MutableLiveData<>();

        rest.getMovieDetails(movieId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RestSubscriber<BaseResponse<MovieDetails>>(liveData) {});

        return liveData;
    }

    public LiveData<BaseResponse<MovieList>> getMoviesPopular(int page) {
        MutableLiveData<BaseResponse<MovieList>> liveData = new MutableLiveData<>();

        rest.getMoviesPopular(REST_LANGUAGE, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RestSubscriber<BaseResponse<MovieList>>(liveData){
                    @Override
                    public void on200(BaseResponse<MovieList> resp) {
                        resp.getResult().setCategory(MovieCategory.POPULAR);
                        liveData.postValue(resp);
                    }
                });

        return liveData;
    }

    public LiveData<BaseResponse<MovieList>> getMoviesTopRated(int page) {
        MutableLiveData<BaseResponse<MovieList>> liveData = new MutableLiveData<>();

        rest.getMoviesTopRated(REST_LANGUAGE, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RestSubscriber<BaseResponse<MovieList>>(liveData) {
                    @Override
                    public void on200(BaseResponse<MovieList> resp) {
                        resp.getResult().setCategory(MovieCategory.TOP_RATED);
                        liveData.postValue(resp);
                    }
                });
        return liveData;
    }

    public LiveData<BaseResponse<MovieList>> getMoviesNowPlaying(int page) {
        MutableLiveData<BaseResponse<MovieList>> liveData = new MutableLiveData<>();

        rest.getMoviesNowPlaying(REST_LANGUAGE, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RestSubscriber<BaseResponse<MovieList>>(liveData) {
                    @Override
                    public void on200(BaseResponse<MovieList> resp) {
                        resp.getResult().setCategory(MovieCategory.NOW_PLAYING);
                        liveData.postValue(resp);
                    }
                });
        return liveData;
    }

    public LiveData<BaseResponse<MovieList>> getMoviesUpcoming(int page) {
        MutableLiveData<BaseResponse<MovieList>> liveData = new MutableLiveData<>();

        rest.getMoviesUpcoming(REST_LANGUAGE, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RestSubscriber<BaseResponse<MovieList>>(liveData) {
                    @Override
                    public void on200(BaseResponse<MovieList> resp) {
                        resp.getResult().setCategory(MovieCategory.UPCOMING);
                        liveData.postValue(resp);
                    }
                });
        return liveData;
    }

    public LiveData<BaseResponse<MovieList>> getSearchMovie(String searchCriteria, int page) {
        MutableLiveData<BaseResponse<MovieList>> liveData = new MutableLiveData<>();

        rest.getMoviesSearch(searchCriteria, REST_LANGUAGE, false, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RestSubscriber<BaseResponse<MovieList>>(liveData) {
                    @Override
                    public void on200(BaseResponse<MovieList> resp) {
                        resp.getResult().setCategory(MovieCategory.SEARCH);
                        super.on200(resp);
                    }
                });

        return liveData;
    }

    //============= Retrofit / OkHttp ==============================================================
    private Interceptor getBaseInterceptor() {
        return chain -> {
            Request request = chain.request();
            okhttp3.Response response = chain.proceed(request);
            String rawJson = response.body().string();

            String resultJson;
            if (response.isSuccessful()) {
                resultJson = String.format(Locale.US, "\"result\":%s", rawJson);
            } else {
                resultJson = String.format(Locale.US, "\"error\":%s", rawJson);
            }

            String finalJson = String.format(Locale.US, "%s\"code\":%d,\"message\":\"%s\",%s%s",
                    "{",
                    response.code(), response.message(), resultJson,
                    "}"
            );

            return response.newBuilder().body(ResponseBody.create(response.body().contentType(), finalJson)).build();
        };
    }

    private OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptorLog = new HttpLoggingInterceptor();
            interceptorLog.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClientBuilder.addInterceptor(interceptorLog);
        }

        okHttpClientBuilder.addInterceptor(getBaseInterceptor());
        return okHttpClientBuilder.build();
    }
}