package borsh.com.tmdbplayground.model.network;

class StatusCode {
    static final int CODE_200_SUCCESS = 200;
    static final int CODE_204_SUCCESS = 204;
    static final int CODE_401_UNAUTHORIZED = 401;
    static final int CODE_404_NOT_FOUND = 404;
}
