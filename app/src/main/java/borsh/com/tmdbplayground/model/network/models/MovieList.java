package borsh.com.tmdbplayground.model.network.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import borsh.com.tmdbplayground.model.network.models.base.BasePagingResponse;
import borsh.com.tmdbplayground.utils.constants.MovieCategory;

public class MovieList extends BasePagingResponse {

    private MovieCategory category;

    @SerializedName("results")
    private List<MovieItem> results;

    public MovieCategory getCategory() {
        return category;
    }

    public void setCategory(MovieCategory category) {
        this.category = category;
    }

    public void setResults(List<MovieItem> results) {
        this.results = results;
    }

    public List<MovieItem> getResults() {
        return results;
    }

    @Override
    public String toString() {
        return
                "MovieList{" +
                        "category = '" + category.getString() + '\'' +
                        ",page = '" + getPage() + '\'' +
                        ",total_pages = '" + getTotalPages() + '\'' +
                        ",results = '" + results + '\'' +
                        ",total_results = '" + getTotalResults() + '\'' +
                        "}";
    }
}