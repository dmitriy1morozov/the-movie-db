package borsh.com.tmdbplayground.model.network;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;

import borsh.com.tmdbplayground.model.network.models.base.BaseResponse;
import retrofit2.HttpException;
import retrofit2.Response;
import timber.log.Timber;

import static borsh.com.tmdbplayground.model.network.StatusCode.CODE_204_SUCCESS;
import static borsh.com.tmdbplayground.model.network.StatusCode.CODE_401_UNAUTHORIZED;
import static borsh.com.tmdbplayground.model.network.StatusCode.CODE_404_NOT_FOUND;


public abstract class RestSubscriber<T> extends DisposableObserverCustom<T> {

    private MutableLiveData<T> mutableLiveData;

    public RestSubscriber(MutableLiveData<T> mutableLiveData) {
        this.mutableLiveData = mutableLiveData;
    }

    //==============================================================================================
    @Override
    public void onStart() {
        Timber.e("RestSubscriber lifecycle onStart");
        super.onStart();
    }

    @Override
    public void onNext(T resp) {
        Timber.e("RestSubscriber lifecycle onNext");
        int statusCode;
        String message;

        if (resp instanceof BaseResponse) {
            BaseResponse baseResponse = (BaseResponse) resp;
            statusCode = baseResponse.getCode();
            message = baseResponse.getMessage();
        } else if (resp instanceof Response) {
            Response response = (Response) resp;
            onOther("Unexpected Retrofit-type Response: " + response);
            return;
        } else {
            onOther("Something went wrong");
            return;
        }

        switch (statusCode) {
            case StatusCode.CODE_200_SUCCESS:
                on200(resp);
                break;
            case CODE_204_SUCCESS:
                on204();
                break;
            default:
                onOther(message);
                break;
        }
    }

    @Override
    public void onError(Throwable e) {
        Timber.e("RestSubscriber lifecycle onError");

        if (e instanceof HttpException) {
            onHttpException((HttpException) e);
        } else if (e instanceof JsonSyntaxException) {
            onResponseModelError(e.getMessage());
        } else {
            onOther(e.getMessage());
        }
    }

    @Override
    public void onSuccess(T t) {
        Timber.e("RestSubscriber lifecycle onSuccess");
        onNext(t);
    }

    @Override
    public void onComplete() {
        Timber.e("RestSubscriber lifecycle onComplete");
    }

    @Override
    public void onDisposed() {
        Timber.e("RestSubscriber lifecycle onDisposed");
    }

    //========= Success handling ===================================================================
    public void on200(T resp) {
        if (mutableLiveData != null) {
            mutableLiveData.postValue(resp);
        }
    }

    public void on204() {
    }

    //========= Error handling =====================================================================
    public void onHttpException(HttpException error) {
        try {
            String errorJson = error.response().errorBody().string();
            Gson gson = new Gson();
            BaseResponse errorResponse = gson.fromJson(errorJson, BaseResponse.class);
            onError4xx(errorResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onError4xx(BaseResponse resp) {
        Timber.e("RestSubscriber onError4xx: %s", resp.toString());
        if (mutableLiveData != null) {
            mutableLiveData.postValue((T) resp);
        }

        switch (resp.getCode()) {
            case CODE_401_UNAUTHORIZED:
                onError401(resp);
                break;
            case CODE_404_NOT_FOUND:
                onError404(resp);
                break;
            default:
                onOther(resp.getMessage());
                break;
        }
    }

    public void onError401(BaseResponse resp) {
        Timber.e("RestSubscriber error! onError401: %s", resp.getMessage());
    }

    public void onError404(BaseResponse resp) {
        Timber.e("RestSubscriber error! onError404: %s", resp.getMessage());
    }

    public void onResponseModelError(String message) {
        Timber.e("RestSubscriber error! onResponseModelError: %s", message);
    }

    public void onOther(String message) {
        Timber.e("RestSubscriber error! onOther: %s", message);
    }
}
