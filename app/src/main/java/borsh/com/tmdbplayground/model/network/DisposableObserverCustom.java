package borsh.com.tmdbplayground.model.network;

import java.util.concurrent.atomic.AtomicReference;

import io.reactivex.MaybeObserver;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.util.EndConsumerHelper;

public abstract class DisposableObserverCustom<T> implements Observer<T>, Disposable, MaybeObserver<T> {

    final AtomicReference<Disposable> disposable = new AtomicReference<Disposable>();

    @Override
    public final void onSubscribe(@NonNull Disposable disposable) {
        if (EndConsumerHelper.setOnce(this.disposable, disposable, getClass())) {
            onStart();
        }
    }

    /**
     * Called once the single upstream Disposable is set via onSubscribe.
     */
    protected void onStart() {
    }

    public abstract void onDisposed();

    @Override
    public final boolean isDisposed() {
        return disposable.get() == DisposableHelper.DISPOSED;
    }

    @Override
    public final void dispose() {
        onDisposed();
        DisposableHelper.dispose(disposable);
    }
}
