package borsh.com.tmdbplayground.view.movies;

import android.animation.Animator;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.FragmentNavigator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.ChangeBounds;
import androidx.transition.ChangeImageTransform;
import androidx.transition.ChangeTransform;
import androidx.transition.TransitionSet;

import com.jakewharton.rxbinding3.widget.RxTextView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import borsh.com.tmdbplayground.R;
import borsh.com.tmdbplayground.databinding.FragmentMovieListBinding;
import borsh.com.tmdbplayground.utils.KeyboardUtils;
import borsh.com.tmdbplayground.utils.constants.MovieCategory;
import borsh.com.tmdbplayground.view.base.MainActivityFragment;
import borsh.com.tmdbplayground.viewmodel.MovieListViewModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import kotlin.Unit;

public class MovieListFragment extends MainActivityFragment<FragmentMovieListBinding> {

    private Disposable disposable;
    private MovieListAdapter adapter;

    private MovieListViewModel viewModel;

    @Override
    protected FragmentMovieListBinding inflate() {
        return FragmentMovieListBinding.inflate(getLayoutInflater());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViewModel();
        observeViewModel();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initTransitions();
        return layout.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initFab();
        initAdapter();
        initRecycler();
    }

    @Override
    public void onStart() {
        super.onStart();
        initSearchBar();
    }

    @Override
    public void onStop() {
        super.onStop();
        disposable.dispose();
    }

    //==============================================================================================
    private void initTransitions() {
        TransitionSet transitionSet = new TransitionSet();
        transitionSet
                .addTransition(new ChangeImageTransform())
                .addTransition(new ChangeBounds())
                .addTransition(new ChangeTransform());

        setSharedElementEnterTransition(transitionSet);
        setSharedElementReturnTransition(transitionSet);

        postponeEnterTransition();
        layout.rvList.getViewTreeObserver().addOnPreDrawListener(() -> {
            startPostponedEnterTransition();
            return true;
        });
    }

    private void initFab() {
        layout.fabMenu.addOnStateChangeListener(isOpened -> {
            if (isOpened) {
                KeyboardUtils.hideKeyboard(layout.getRoot());
            }
            return Unit.INSTANCE;
        });

        layout.fabMenu.addOnMenuItemClickListener((fab, textView, itemId) -> {
            MovieCategory category;
            switch (itemId) {
                case R.id.menuPopular:
                    hideSearchBar();
                    category = MovieCategory.POPULAR;
                    break;
                case R.id.menuTopRated:
                    hideSearchBar();
                    category = MovieCategory.TOP_RATED;
                    break;
                case R.id.menuNowPlaying:
                    hideSearchBar();
                    category = MovieCategory.NOW_PLAYING;
                    break;
                case R.id.menuUpcoming:
                    hideSearchBar();
                    category = MovieCategory.UPCOMING;
                    break;
                case R.id.menuSearch:
                    showSearchBar();
                    layout.etSearch.requestFocus();
                    return Unit.INSTANCE;
                default:
                    return Unit.INSTANCE;
            }

            viewModel.selectCategory(category);
            return Unit.INSTANCE;
        });
    }

    private void initAdapter() {
        if (adapter != null) {
            return;
        }
        adapter = new MovieListAdapter(getResources(),
                (movieItem, transitionView) -> {
                    Bundle args = new Bundle();
                    args.putParcelable(getString(R.string.args_movie_item), movieItem);

                    FragmentNavigator.Extras extras = new FragmentNavigator.Extras.Builder()
                            .addSharedElement(transitionView, transitionView.getTransitionName())
                            .build();
                    navigate.to(R.id.nav_movieList_poster, args, extras);
                },
                (movieItem, transitionView) -> {
                    Bundle args = new Bundle();
                    args.putParcelable(getString(R.string.args_movie_item), movieItem);

                    FragmentNavigator.Extras extras = new FragmentNavigator.Extras.Builder()
                            .addSharedElement(transitionView, transitionView.getTransitionName())
                            .build();
                    navigate.to(R.id.nav_movieList_movieDetails, args, extras);
                });
    }

    private void initRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layout.rvList.setLayoutManager(layoutManager);
        layout.rvList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        layout.rvList.setAdapter(adapter);

        layout.rvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int lastVisiblePosition = layoutManager.findLastCompletelyVisibleItemPosition();
                if (MovieListFragment.this.isResumed()
                        && viewModel.getItemCount() != 0
                        && lastVisiblePosition == adapter.getItemCount() - 1) {
                    viewModel.doPagination();
                }
            }
        });
    }

    private void initSearchBar() {
        layout.flSearchClear.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(layout.etSearch.getText())) {
                layout.etSearch.setText("");
            }
        });

        layout.flSearchClose.setOnClickListener(view -> {
            hideSearchBar();
        });

        RxTextView.textChanges(layout.etSearch)
                .skip(2)
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread()).
                subscribe(new Observer<CharSequence>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(CharSequence charSequence) {
                        String searchCriteria = charSequence.toString().trim();
                        if (TextUtils.isEmpty(searchCriteria)) {
                            viewModel.setPreviousCategory();
                        } else {
                            viewModel.searchMovies(charSequence.toString());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private void showSearchBar() {
        int x = layout.fabMenu.getRight();
        int y = layout.fabMenu.getBottom();
        int startRadius = 0;
        int endRadius = (int) Math.hypot(layout.getRoot().getWidth(), layout.getRoot().getHeight());

        Animator anim = ViewAnimationUtils.createCircularReveal(layout.llSearchBar, x, y, startRadius, endRadius);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                KeyboardUtils.showKeyboard(layout.etSearch);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        layout.llSearchBar.setVisibility(View.VISIBLE);
        anim.start();
    }

    private void hideSearchBar() {
        int x = layout.fabMenu.getRight();
        int y = layout.fabMenu.getBottom();
        int startRadius = (int) Math.hypot(layout.getRoot().getWidth(), layout.getRoot().getHeight());
        int endRadius = 0;

        Animator anim = ViewAnimationUtils.createCircularReveal(layout.llSearchBar, x, y, startRadius, endRadius);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                layout.llSearchBar.setVisibility(View.GONE);
                KeyboardUtils.hideKeyboard(layout.getRoot());
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        anim.start();
    }

    //========= ViewModel ==========================================================================
    private void initViewModel() {
        viewModel = ViewModelProviders.of(this).get(MovieListViewModel.class);
        viewModel.selectCategory(MovieCategory.POPULAR);
    }

    private void observeViewModel() {
        viewModel.getIsLoadingObservable().observe(this, this::showLoader);

        viewModel.getErrorObservable().observe(this, baseResponse -> showNetworkError(baseResponse, new RetryListener() {
            @Override
            public void retry() {
                viewModel.retry();
            }

            @Override
            public void cancel() {
                // TODO: Show some placeHolder
                showToast("Canceled");
            }
        }));

        viewModel.getCategoryObservable().observe(this, category -> {
            if (category == null) {
                return;
            }
            layout.rvList.scrollToPosition(0);
        });

        viewModel.getMovieListObservable().observe(this, movieItems -> {
            adapter.setList(new ArrayList<>(movieItems));
            checkPlaceHolder();
        });
    }

    private void checkPlaceHolder() {
        if (adapter.getItemCount() == 0) {
            layout.tvPlaceholder.setVisibility(View.VISIBLE);
        } else {
            layout.tvPlaceholder.setVisibility(View.GONE);
        }
    }
}