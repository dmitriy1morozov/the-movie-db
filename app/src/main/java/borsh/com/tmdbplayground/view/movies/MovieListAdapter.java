package borsh.com.tmdbplayground.view.movies;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import borsh.com.tmdbplayground.R;
import borsh.com.tmdbplayground.databinding.ItemMovieBinding;
import borsh.com.tmdbplayground.model.network.models.MovieItem;
import borsh.com.tmdbplayground.utils.Utils;
import borsh.com.tmdbplayground.utils.constants.PosterSize;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.ItemVh> {

    private List<MovieItem> items;
    private MovieClickListener posterClickListener;
    private MovieClickListener movieClickListener;

    private Resources resources;

    interface MovieClickListener {
        void onMovieClicked(MovieItem movieItem, View transitionView);
    }

    MovieListAdapter(Resources resources, MovieClickListener posterClickListener, MovieClickListener movieClickListener) {
        this.items = new ArrayList<>();
        this.posterClickListener = posterClickListener;
        this.movieClickListener = movieClickListener;
        this.resources = resources;
    }

    @NonNull
    @Override
    public ItemVh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemVh(ItemMovieBinding.inflate(LayoutInflater.from(parent.getContext())));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemVh holder, int position) {
        MovieItem movie = items.get(position);

        Utils.setRoundCorners(holder.layout.ivPoster);
        String posterPath = movie.getPosterPath();
        String posterPathUri = resources.getString(R.string.rest_base_url_poster, PosterSize.w185.toString(), posterPath);

        RequestOptions requestOptions = RequestOptions.placeholderOf(R.drawable.placeholder_movie).dontTransform();
        Glide.with(holder.itemView.getContext())
                .load(posterPathUri)
                .apply(requestOptions)
                .onlyRetrieveFromCache(true)
                .into(holder.layout.ivPoster);

        holder.layout.tvTitle.setText(movie.getTitle());
        holder.layout.tvPopularity.setText(resources.getString(R.string.txt_popularity, movie.getPopularity()));

        holder.layout.ivPoster.setTransitionName(movie.getOriginalTitle());
        holder.layout.ivPoster.setOnClickListener(v -> posterClickListener.onMovieClicked(movie, holder.layout.ivPoster));
        holder.layout.getRoot().setOnClickListener(v -> movieClickListener.onMovieClicked(movie, holder.layout.ivPoster));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    //==============================================================================================
    void setList(List<MovieItem> itemsNew) {
        MovieListDiffUtil diffCallback = new MovieListDiffUtil(items, itemsNew);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback, true);

        items = itemsNew;
        diffResult.dispatchUpdatesTo(MovieListAdapter.this);
    }

    //==============================================================================================
    class ItemVh extends RecyclerView.ViewHolder {
        ItemMovieBinding layout;

        ItemVh(ItemMovieBinding itemView) {
            super(itemView.getRoot());
            this.layout = itemView;
        }
    }
}
