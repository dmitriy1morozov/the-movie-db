package borsh.com.tmdbplayground.view.movies;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.FragmentNavigator;
import androidx.transition.ChangeBounds;
import androidx.transition.ChangeImageTransform;
import androidx.transition.ChangeTransform;
import androidx.transition.TransitionSet;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.jetbrains.annotations.NotNull;

import borsh.com.tmdbplayground.R;
import borsh.com.tmdbplayground.databinding.FragmentMovieDetailsBinding;
import borsh.com.tmdbplayground.model.network.models.MovieDetails;
import borsh.com.tmdbplayground.model.network.models.MovieItem;
import borsh.com.tmdbplayground.utils.Utils;
import borsh.com.tmdbplayground.utils.constants.PosterSize;
import borsh.com.tmdbplayground.view.base.MainActivityFragment;
import borsh.com.tmdbplayground.viewmodel.MovieDetailsViewModel;

public class MovieDetailsFragment extends MainActivityFragment<FragmentMovieDetailsBinding> {

    private MovieDetailsViewModel viewModel;
    private MovieItem movieItem;

    @Override
    protected FragmentMovieDetailsBinding inflate() {
        return FragmentMovieDetailsBinding.inflate(getLayoutInflater());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args == null ||
                !args.containsKey(getString(R.string.args_movie_item)) ||
                args.getParcelable(getString(R.string.args_movie_item)) == null) {
            showToast(getString(R.string.error_general));
            navigate.back();
            return;
        } else {
            movieItem = args.getParcelable(getString(R.string.args_movie_item));
        }

        initViewModel();
        observeViewModel(viewModel);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Utils.setRoundCorners(layout.ivPoster);
        initTransitions();
        loadPreviewImage();

        return layout.getRoot();
    }

    private void initTransitions() {
        layout.ivPoster.setTransitionName(movieItem.getOriginalTitle());

        TransitionSet transitionSet = new TransitionSet();
        transitionSet
                .addTransition(new ChangeImageTransform())
                .addTransition(new ChangeBounds())
                .addTransition(new ChangeTransform());

        setSharedElementEnterTransition(transitionSet);
        setSharedElementReturnTransition(transitionSet);

        postponeEnterTransition();
    }

    private void loadPreviewImage() {
        String posterPathUri = getString(R.string.rest_base_url_poster, PosterSize.w185.toString(), movieItem.getPosterPath());

        RequestOptions requestOptions = new RequestOptions().dontTransform();
        Glide.with(this)
                .load(posterPathUri)
                .apply(requestOptions)
                .onlyRetrieveFromCache(true)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        startPostponedEnterTransition();
                        return false;
                    }

                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        startPostponedEnterTransition();
                        return false;
                    }
                })
                .into(layout.ivPoster);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.fetchMovieDetails(movieItem.getId());
    }

    //==============================================================================================

    private void initViewModel() {
        viewModel = ViewModelProviders.of(this).get(MovieDetailsViewModel.class);
    }

    private void observeViewModel(MovieDetailsViewModel viewModel) {
        viewModel.getIsLoadingObservable().observe(this, this::showLoader);

        viewModel.getErrorObservable().observe(this, baseResponse -> {
            showNetworkError(baseResponse, new RetryListener() {
                @Override
                public void retry() {
                    viewModel.retry();
                }

                @Override
                public void cancel() {
                    // TODO: Show some placeHolder
                    showToast("Canceled");
                }
            });
        });

        viewModel.getMovieDetailsObservable().observe(this, movieDetails -> {
            if (movieDetails != null) {
                dispatchDetailsToUi(movieDetails);
            }
        });
    }

    private void dispatchDetailsToUi(MovieDetails movieDetails) {
        if (isAdded() && isResumed()) {
            layout.ivPoster.setOnClickListener(view -> {
                Bundle args = new Bundle();
                args.putParcelable(getString(R.string.args_movie_item), movieItem);

                FragmentNavigator.Extras extras = new FragmentNavigator.Extras.Builder()
                        .addSharedElement(view, view.getTransitionName())
                        .build();
                navigate.to(R.id.nav_movieDetails_poster, args, extras);
            });

            layout.tvTitle.setText(movieDetails.getOriginalTitle());
            layout.tvPopularity.setText(getString(R.string.txt_popularity, movieDetails.getPopularity()));
            layout.tvBudget.setText(getString(R.string.txt_budget, movieDetails.getBudget()));

            String homePage = movieDetails.getHomepage();
            layout.tvHomePage.setText(homePage);
            layout.tvHomePage.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(homePage));
                startActivity(intent);
            });

            layout.tvOverview.setText(movieDetails.getOverview());
        }
    }
}
