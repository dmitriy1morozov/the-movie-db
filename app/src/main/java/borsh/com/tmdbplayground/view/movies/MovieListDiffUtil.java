package borsh.com.tmdbplayground.view.movies;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

import borsh.com.tmdbplayground.model.network.models.MovieItem;

public class MovieListDiffUtil extends DiffUtil.Callback {

    private final List<MovieItem> oldList;
    private final List<MovieItem> newList;

    MovieListDiffUtil(List<MovieItem> oldList, List<MovieItem> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList != null ? oldList.size() : 0;
    }

    @Override
    public int getNewListSize() {
        return newList != null ? newList.size() : 0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        MovieItem oldItem = oldList.get(oldItemPosition);
        MovieItem newItem = newList.get(newItemPosition);

        return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        MovieItem oldItem = oldList.get(oldItemPosition);
        MovieItem newItem = newList.get(newItemPosition);

        return oldItem.getId() == newItem.getId()
                && oldItem.getTitle().equals(newItem.getTitle())
                && oldItem.getOriginalTitle().equals(newItem.getOriginalTitle());
    }
}
