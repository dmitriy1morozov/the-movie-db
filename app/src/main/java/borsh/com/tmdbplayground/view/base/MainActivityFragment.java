package borsh.com.tmdbplayground.view.base;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewbinding.ViewBinding;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

import borsh.com.tmdbplayground.R;
import borsh.com.tmdbplayground.TmdbNavigation;
import borsh.com.tmdbplayground.model.network.models.base.BaseResponse;

public abstract class MainActivityFragment<FragmentViewBinding extends ViewBinding> extends Fragment {

    protected FragmentViewBinding layout;
    protected TmdbNavigation navigate;

    protected Dialog posterDialog;

    private ProgressBar loader;
    private CurrentFragmentListener currentFragmentListener;

    public interface CurrentFragmentListener {
        void setCurrentFragmentRef(MainActivityFragment mainActivityFragment);
    }

    public interface RetryListener {
        void retry();

        void cancel();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layout = inflate();
        navigate = TmdbNavigation.initWihtFragment(layout);
    }

    protected abstract FragmentViewBinding inflate();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (!(getActivity() instanceof CurrentFragmentListener)) {
            throw new IllegalStateException("Activity must implement CurrentFragmentListener.");
        }
        currentFragmentListener = (CurrentFragmentListener) context;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return layout.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initLoader(view);
    }

    @Override
    public void onStart() {
        super.onStart();
        currentFragmentListener.setCurrentFragmentRef(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        showLoader(false);
    }

    //==============================================================================================

    private void initLoader(View fragmentView) {
        loader = new ProgressBar(getContext());
        loader.setIndeterminate(true);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;
        FrameLayout rootFrame = fragmentView.findViewById(R.id.rootFrame);
        if (rootFrame != null) {
            rootFrame.addView(loader, layoutParams);
            loader.setVisibility(View.GONE);
        }
    }

    //==============================================================================================

    protected MainActivity getParentActivity() {
        return (MainActivity) getActivity();
    }

    protected void showNetworkError(BaseResponse baseResponse, RetryListener networkRetryListener) {
        String errorString = String.format(Locale.US, "%d: %s\nMessage: %s",
                baseResponse.getCode(), baseResponse.getMessage(), baseResponse.getError().getStatusMessage());
        showToast(errorString);

        if (isAdded() && isResumed()) {
            Snackbar networkError = Snackbar.make(layout.getRoot(), R.string.error_retrieve_data_failed, Snackbar.LENGTH_LONG);
            networkError.setAction(R.string.btn_retry, v -> networkRetryListener.retry());
            networkError.addCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar transientBottomBar, int event) {
                    super.onDismissed(transientBottomBar, event);
                    if (isAdded() && isResumed()) {
                        networkRetryListener.cancel();
                    }
                }
            });
            networkError.show();
        }
    }

    protected void showToast(String message) {
        MainActivity mainActivity = getParentActivity();
        if (mainActivity != null) {
            mainActivity.showToast(message);
        }
    }

    protected void showLoader(boolean isLoading) {
        if (isLoading) {
            loader.setVisibility(View.VISIBLE);
        } else {
            loader.setVisibility(View.GONE);
        }
    }
}
