package borsh.com.tmdbplayground.view.base;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.SharedElementCallback;
import androidx.viewbinding.ViewBinding;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import borsh.com.tmdbplayground.R;
import borsh.com.tmdbplayground.TmdbNavigation;
import borsh.com.tmdbplayground.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements MainActivityFragment.CurrentFragmentListener {

    private TmdbNavigation navigate;
    private MainActivityFragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewBinding layout = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(layout.getRoot());

        navigate = TmdbNavigation.initWihtActivity(this, R.id.navHostFragment);

        // TODO: 09.10.2019 May be redundant Doesn't work as intended yet
        setExitSharedElementCallback(new SharedElementCallback() {
            @Override
            public void onSharedElementEnd(List<String> sharedElementNames, List<View> sharedElements, List<View> sharedElementSnapshots) {
                super.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots);
                for (View view : sharedElements) {
                    if (view instanceof SimpleDraweeView) {
                        ((SimpleDraweeView) view).getDrawable().setVisible(true, true);
                    }
                }
            }
        });
    }

    //==============================================================================================

    @Override
    public void setCurrentFragmentRef(MainActivityFragment mainActivityFragment) {
        currentFragment = mainActivityFragment;
    }

    public void showToast(String message) {
        if (!this.isFinishing()) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
    }
}
