package borsh.com.tmdbplayground.view.movies;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.transition.ChangeBounds;
import androidx.transition.ChangeImageTransform;
import androidx.transition.ChangeTransform;
import androidx.transition.Transition;
import androidx.transition.TransitionSet;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.jetbrains.annotations.NotNull;

import borsh.com.tmdbplayground.R;
import borsh.com.tmdbplayground.databinding.FragmentPosterBinding;
import borsh.com.tmdbplayground.model.network.models.MovieItem;
import borsh.com.tmdbplayground.utils.constants.PosterSize;
import borsh.com.tmdbplayground.view.base.MainActivityFragment;

public class PosterFragment extends MainActivityFragment<FragmentPosterBinding> {

    private MovieItem movieItem;

    @Override
    protected FragmentPosterBinding inflate() {
        return FragmentPosterBinding.inflate(getLayoutInflater());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args == null) {
            navigate.back();
        } else {
            movieItem = args.getParcelable(getString(R.string.args_movie_item));
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        initTransitions();
        loadPreviewImage();

        return layout.getRoot();
    }

    private void initTransitions() {
        layout.ivPoster.setTransitionName(movieItem.getOriginalTitle());

        TransitionSet transitionSet = new TransitionSet();
        transitionSet
                .addTransition(new ChangeImageTransform())
                .addTransition(new ChangeBounds())
                .addTransition(new ChangeTransform());

        setSharedElementEnterTransition(transitionSet);
        setSharedElementReturnTransition(transitionSet);

        postponeEnterTransition();

        transitionSet.addListener(new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(@NonNull Transition transition) {
                loadPreviewImage();
            }
            @Override
            public void onTransitionEnd(@NonNull Transition transition) {
                if (PosterFragment.this.isResumed()) {
                    loadOriginalSizeImage();
                }
            }
            @Override
            public void onTransitionCancel(@NonNull Transition transition) {
            }
            @Override
            public void onTransitionPause(@NonNull Transition transition) {
            }
            @Override
            public void onTransitionResume(@NonNull Transition transition) {
            }
        });
    }

    private void loadPreviewImage() {
        String previewPosterPathUri = getString(R.string.rest_base_url_poster, PosterSize.w185.toString(), movieItem.getPosterPath());

        RequestOptions requestOptions = new RequestOptions().dontTransform();
        Glide.with(this)
                .load(previewPosterPathUri)
                .apply(requestOptions)
                .onlyRetrieveFromCache(true)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        startPostponedEnterTransition();
                        return false;
                    }

                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        startPostponedEnterTransition();
                        return false;
                    }
                })
                .into(layout.ivPoster);
    }

    private void loadOriginalSizeImage() {
        String originalPosterPathUri = getString(R.string.rest_base_url_poster, PosterSize.original.toString(), movieItem.getPosterPath());
        Glide.with(PosterFragment.this)
                .load(originalPosterPathUri)
                .placeholder(layout.ivPoster.getDrawable())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(layout.ivPoster);
    }
}
