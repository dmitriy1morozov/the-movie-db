package borsh.com.tmdbplayground;

import com.facebook.drawee.backends.pipeline.Fresco;

import timber.log.Timber;

public class TmdbApplication extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        Fresco.initialize(this);
    }
}
